# backup-script

This is a small python script deployable as a Helm chart to back up Mathezirkel server content.

## Usage

`backup-script` is a tool allowing backing up and restoring of the following tools according to their documentations:

* K3s (see https://docs.k3s.io/datastore/backup-restore)
* Keycloak (see https://www.keycloak.org/docs/21.1.1/upgrading/#intro)
* Nextcloud (see https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html)
* Hedgedoc (see https://docs.hedgedoc.org/setup/docker/#backup)
* Web server static files (large files contained in a persistent volume on the Kubernetes cluster)

The script works by saving all relevant data to files and pushing them to a configurable (possibly remote) location
using [restic](https://github.com/restic/restic).

## License

Source code and binaries are licensed under GPL-3, see [LICENSE]().
